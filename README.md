# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | N         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 

    Describe how to use your web and maybe insert images to help you explain.

    打開網頁可以看到右邊為工具列，而上方有顏色及筆刷大小的選擇，選擇顏色點開下方框框，選擇顏色後按畫布旁的空白處，但方形匡顏色改變即完成選色。

    介紹工具列(矩陣編號a[4][3])：滑鼠點下即可啟用功能
    1. 第一列：筆刷(a[0][0])、橡皮擦(a[0][1])、文字(a[0][2])
    2. 第二列：圖形繪製(圓形a[1][0]、三角形a[1][1]、矩形a[1][2])
    		  [註]：在繪製圖案時，由於隨著滑鼠的移動拖移圖形大小，需要緩慢拖移圖形才會填滿所指定顏色。

    3. 第三列：復原(a[2][0])、取消復原(a[2][1])、清除畫布(a[2][2])
    4. 第四列：儲存目前畫布狀態(a[3][0])、下載圖片(a[3][1])、載入圖片(a[3][2])
    		  [註]：載入圖片請按上方長型按鈕"選擇檔案"選擇欲載入的圖片。

    Cursor icon: 按下工具列的工具，滑鼠移至畫布時之游標改變。
    1. 滑鼠游標會變成"十字狀"：按工具列中的 "鉛筆" 以及 "圖形"（圓形、三角形、矩形)。
    2. 滑鼠游標會變成"抓取"的圖案：按下工具列中的"橡皮擦"。
    3. 滑鼠游標會變成"文字輸入"樣：按下工具列中的"文字"。
    4. 滑鼠游標會變回"預設"的游標：按下工具列中的 "清除畫布"。

### Function description

    Decribe your bonus function and how to use it.


### Gitlab page link

    your web page URL, which should be "https://[studentID].gitlab.io/AS_01_WebCanvas"

    https://105000036.gitlab.io/AS_01_WebCanvas

### Others (Optional)

    Anything you want to say to TAs.

<style>
table th{
    width: 100%;
}
</style>