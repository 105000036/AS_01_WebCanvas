console.log("Hello!");

// SETTING ALL VARIABLES
var canvas = document.getElementById('myCanvas');
var ctx = canvas.getContext('2d');
var isMouseDown=false;

var body = document.getElementsByTagName("body")[0];
var linesArray = [];
var circle = 0, rectangle = 0, triangle=0;
var currentPosition;
let TextX = -100;
let TextY = -100;

currentSize = 5;
var currentColor = "rgb(200,20,100)"; 
document.getElementById('colorpicker').addEventListener('change', function() {
	currentColor = this.value;
});

document.getElementById('controlSize').addEventListener('change', function() {
	currentSize = this.value;
	document.getElementById("canvas").innerHTML = this.value;
});

/*undo redo*/
//start listening to mouse moving 
canvas.addEventListener('mousedown', function(evt) {
  isMousedown = true;
  // md = true;
  currentPosition = getMousePos(canvas, evt);
  ctx.beginPath();
  ctx.moveTo(currentPosition.x, currentPosition.y);
  evt.preventDefault();
  console.log("isMousedown 1: ", isMousedown);
  canvas.addEventListener('mousemove', mousemove, false);
});

//stop listening to mouse
canvas.addEventListener('mouseup', function() {
  console.log("mouseup");
  isMousedown = false;
  // md = false;
  rectangle = 0;
  circle = 0;
  triangle = 0 ;
  var state = ctx.getImageData(0, 0, canvas.width, canvas.height);
  window.history.pushState(state, null);

  canvas.removeEventListener('mousemove', mousemove, false);
}, false);

// GET MOUSE POSITION
function getMousePos(canvas, evt) {
	var rect = canvas.getBoundingClientRect();
	return {
		x: evt.clientX - rect.left,
		y: evt.clientY - rect.top
	};
}
// ON MOUSE DOWN
function mousedown(canvas, evt) {
	currentPosition = getMousePos(canvas, evt);
	isMouseDown=true;
	currentPosition = getMousePos(canvas, evt);
	ctx.moveTo(currentPosition.x, currentPosition.y)
	ctx.beginPath();
	ctx.lineWidth  = currentSize;
	ctx.lineCap = "round";
	ctx.strokeStyle = currentColor;
	
}


// ON MOUSE MOVE
function mousemove(canvas, evt) {
	console.log("isMouseDown: ", isMouseDown);
	if(isMouseDown){
		currentPosition = getMousePos(canvas, evt);
		// cx = currentPosition.x;
		// cy = currentPosition.y;

		if(mode==1){  //pencil
			ctx.globalCompositeOperation="source-over";
			ctx.lineTo(currentPosition.x, currentPosition.y);
			ctx.stroke();

		}else if(mode==2){  //eraser
			ctx.lineTo(currentPosition.x, currentPosition.y);
			ctx.globalCompositeOperation="destination-out";
			ctx.stroke();

		}else if(mode==3){  //text
		    ctx.globalCompositeOperation="source-over";

			// var createtextbox = document.createElement("INPUT");
			// createtextbox.setAttribute("type", "text");


		    // ctx.fillStyle = "rgba(10,10,10)";
		    // if(rectangle == 0){
		    //     cx = currentPosition.x;
		    //     cy = currentPosition.y;
		    //     rectangle = 1;
		    //     TextX = cx+15;

		    //     let k = currentSize; //k:0~50
		    //     ctx.fillRect(cx+k,cy+k,100+3*k,1.1*k);
		    //     ctx.clearRect(cx+0.3*k,cy+0.3*k,100+2.5*k,1.25*k)
		    //     TextY = cy+k;
		    // }

		}else if(mode==4){ //circle
			console.log("in circle");
			ctx.globalCompositeOperation="source-over";
			// ctx.arc(100,100, 50,0,Math.PI*2);
	        // ctx.fill();
	       if(circle == 0){
	            cx = currentPosition.x;
	            cy = currentPosition.y;
	            circle = 1;
	       }
	       if(circle == 1){
	            ctx.arc(cx,cy, Math.abs(currentPosition.x-cx)/2,0,Math.PI*2);
				ctx.stroke();
				//ctx.fill();
				
	       }
		}else if(mode ==5){ //triangle
			ctx.globalCompositeOperation="source-over";
			if(triangle == 0){
		        cx = currentPosition.x;
		        cy = currentPosition.y;
		        ctx.moveTo(cx,cy);
		        triangle = 1;
		    }
		    if(triangle == 1){
				ctx.moveTo(cx,cy);
		        ctx.lineTo(2*cx-currentPosition.x,currentPosition.y);
		        ctx.lineTo(currentPosition.x,currentPosition.y);
		        ctx.stroke();
				//ctx.fill();	
		    }
		}else if(mode == 6){  //rectangle
			ctx.globalCompositeOperation="source-over";
		    if(rectangle == 0){
		        cx = currentPosition.x;
		        cy = currentPosition.y;
		        rectangle = 1;
		    }
		    if(rectangle == 1){
		        ctx.fillRect(cx,cy,currentPosition.x-cx,currentPosition.y-cy);		
				//ctx.stoke();		
		    }				
		} 
		store(currentPosition.x, currentPosition.y, currentSize, currentColor);	
	}
}

// STORE DATA
function store(x, y, s, c) {
	var line = {
		"x": x,
		"y": y,
		"size": s,
		"color": c
	}
	linesArray.push(line);
}
// ON MOUSE UP
function mouseup() {
	isMouseDown=false;
	store();
}

//row2
function pencil(){
	mode = 1;
	canvas.addEventListener('mousedown', function() {mousedown(canvas, event);});
	canvas.addEventListener('mousemove',function() {mousemove(canvas, event);});
	canvas.addEventListener('mouseup',mouseup);
	console.log("pencil");
	document.body.style.cursor = 'crosshair';
}

function eraser(){
	mode = 2;
	console.log("eraser");
	document.body.style.cursor = 'grabbing';
	// currentSize = 50;
	// ctx.fillStyle = currentColor

}

function text(){
	mode = 3;
    document.body.style.cursor = 'text';
    console.log("text");


  	// var coolText = document.createElement("div");
  	// coolText.innerHTML = document.getElementById("myCanvas").value;
  	// document.getElementById("coolText").appendChild(coolText1)
}

//row3
function circle1(){
    mode = 4;
    canvas.addEventListener('mousedown', function() {mousedown(canvas, event);});
	canvas.addEventListener('mousemove',function() {mousemove(canvas, event);});
	canvas.addEventListener('mouseup',mouseup);
    document.body.style.cursor = 'cell';
    console.log("circle");
}

function triangle1(){
	mode = 5;
	canvas.addEventListener('mousedown', function() {mousedown(canvas, event);});
	canvas.addEventListener('mousemove',function() {mousemove(canvas, event);});
	canvas.addEventListener('mouseup',mouseup);
	document.body.style.cursor = 'cell';
	console.log("triangle");
}

function rectangle1(){
	mode = 6;
	canvas.addEventListener('mousedown', function() {mousedown(canvas, event);});
	canvas.addEventListener('mousemove',function() {mousemove(canvas, event);});
	canvas.addEventListener('mouseup',mouseup);
    document.body.style.cursor = 'cell';
    console.log("rectangle");
}

//row3
//row3
//undo redo
let state = ctx.getImageData(0, 0, canvas.width, canvas.height);
window.history.pushState(state, null);
window.addEventListener('popstate', changeStep, false);

function changeStep(e){
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  if( e.state ){
    ctx.putImageData(e.state, 0, 0);
  }
}
function Undo(){
	history.go(-1);
}

function Redo(){
	history.go(1);
}

//clear
function clr(){
    ctx.clearRect(0, 0, 700, 500);
    document.body.style.cursor = 'default';
    console.log("Clear!");
	// localStorage.removeItem("myCanvas");
	// linesArray = [];
 	// console.log("Cache cleared!");
}

//row4
//save 
function save1(){
	localStorage.removeItem("myCanvas");
	localStorage.setItem("myCanvas", JSON.stringify(linesArray));
	console.log("Saved canvas!");
}

// DOWNLOAD CANVAS
download_img = function(el) {
  var image = canvas.toDataURL("image/png");
  el.href = image;
};

//upload
var imageLoader = document.getElementById('btn_upload');
imageLoader.addEventListener('change', upload, false);
function upload(e){
	console.log("upload");
	var imageLoader = URL.createObjectURL(e.target.files[0]);
	var Pic = new Image();
	Pic.src = imageLoader;
	Pic.onload = function(){
		ctx.drawImage(Pic,0,0);
	}
}


	

